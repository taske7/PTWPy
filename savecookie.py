import pickle
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def save_cookie(driver, path):
    with open(path, 'wb') as filehandler:
        pickle.dump(driver.get_cookies(), filehandler)
    print('cookie saved at ' + path)


# substitute as appropriate
chromedriver = '/Users/ishandutta2007/Downloads/chromedriver'
driver = webdriver.Chrome(chromedriver)
fb_url = "https://www.facebook.com/login.php"
driver.get(fb_url)

inputElement = driver.find_element_by_id("email")
inputElement.send_keys('ishandutta2007')
inputElement = driver.find_element_by_id("pass")
inputElement.send_keys('iamacoder21')
inputElement.send_keys(Keys.ENTER)

save_cookie(driver, '/tmp/pramolta_fb_cookie')
time.sleep(2)

goog_url = "https://accounts.google.com/signin/v2/identifier"
driver.get(goog_url)

inputElement2 = driver.find_element_by_id("identifierId")
inputElement2.send_keys('ishandutta2007')
inputElement2.send_keys(Keys.ENTER)

time.sleep(2)

inputElement2 = driver.find_element_by_xpath(
    "//*[@id='password']/div[1]/div/div[1]/input")
inputElement2.send_keys('iamagandu')
inputElement2.send_keys(Keys.ENTER)

save_cookie(driver, '/tmp/pramolta_google_cookie')
driver.quit()
