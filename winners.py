import pickle
import time
import re
import csv

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, UnexpectedAlertPresentException, WebDriverException
from selenium.webdriver.common.action_chains import ActionChains
from random import randint
from bs4 import BeautifulSoup

rid = "2377"
network_latency = 0
rows = []
chromedriver = '/Users/ishandutta2007/Downloads/chromedriver'


def load_cookie(driver, path):
    with open(path, 'rb') as cookiesfile:
        cookies = pickle.load(cookiesfile)
        for cookie in cookies:
            driver.add_cookie(cookie)


def login(driver):
    time.sleep(randint(3, 5) + network_latency)
    try:
        driver.find_element_by_xpath(
            "//*[@id='myModal']/div/div/div[2]/div/div/div/div/div[3]/h2/a").click()
    except NoSuchElementException:
        print(f'You are already logged in')


def click_winners(driver):
    elem = driver.find_element_by_xpath(f'//*[@id="block-{rid}"]/div[3]/a')
    actions = ActionChains(driver)
    actions.click(elem).perform()
    time.sleep(5)


def scroll(driver):
    global rows
    for i in range(1, 3):
        query = "$('#winnersModal').animate({scrollTop : " + \
            str(i * 10000) + "}, 'slow');"
        print(query)
        driver.execute_script(query)
        time.sleep(2)

    source = driver.page_source
    parsed_html = BeautifulSoup(source, 'html.parser')
    rows = parsed_html.body.findAll('tr')
    n_junk_begin, n_junk_end = 30 + 28, 5
    rows = rows[n_junk_begin:]
    rows = rows[:len(rows) - n_junk_end]


def get_text(ele):
    if (ele is None):
        return '-'
    else:
        return re.sub(' +', ' ', ele.text)


def dump_winners(driver):
    with open(f'winners/winners_{rid}.csv', 'w') as f:
        w = csv.writer(f)
        for i in range(0, len(rows)):
            name_ele = rows[i].find('span', attrs={'class': 'avtar-title'})
            earnings_ele = rows[i].find('span', attrs={'class': 'price-value'})
            w.writerow([i, get_text(name_ele), get_text(earnings_ele)])


def do():
    driver = webdriver.Chrome(chromedriver)
    try:
        driver.get("https://www.baymack.com/winners")
        load_cookie(driver, '/tmp/cookie')
        login(driver)
        click_winners(driver)
        scroll(driver)
        dump_winners(driver)
    except (UnexpectedAlertPresentException, WebDriverException, BaseException) as e:
        print(e)
    driver.quit()


if __name__ == "__main__":
    do()
