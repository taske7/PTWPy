import os
for i in range(1, 10):
    for j in range(1, 10):
        if i != j:
            try:
                with open(f'3dim/3dim_{i}_to_{j}.txt') as infile:
                    try:
                        os.remove(f'3dim_offset/3dim_{i}_to_{j}_offset.txt')
                    except OSError:
                        pass
                    with open(f'3dim_offset/3dim_{i}_to_{j}_offset.txt', 'a') as outfile:
                        x0, y0 = -1, -1
                        for line in infile:
                            x, y = line[1:-2].partition(",")[::2]
                            if x0 == -1 and y0 == -1:
                                x0, y0 = int(x), int(y)
                            outfile.write(
                                '(' + str(int(x) - x0) + ', ' + str(int(y) - y0) + ')\n')
            except Exception as e:
                print(e)

    try:
        with open(f'3dim/3dim_{i}_to_verify.txt') as infile:
            try:
                os.remove(f'3dim_offset/3dim_{i}_to_verify_offset.txt')
            except OSError:
                pass
            with open(f'3dim_offset/3dim_{i}_to_verify_offset.txt', 'a') as outfile:
                x0, y0 = -1, -1
                for line in infile:
                    x, y = line[1:-2].partition(",")[::2]
                    if x0 == -1 and y0 == -1:
                        x0, y0 = int(x), int(y)
                    outfile.write(
                        '(' + str(int(x) - x0) + ', ' + str(int(y) - y0) + ')\n')
    except Exception as e:
        print(e)
