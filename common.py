import datetime
import pickle
import re
import sqlite3
import sys
import threading
import time

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, ElementNotVisibleException, UnexpectedAlertPresentException, WebDriverException
from selenium.webdriver.common.action_chains import ActionChains
from random import randint
from bs4 import BeautifulSoup


class CustomError(Exception):
    pass


SITES = [
    {
        "url": "https://www.baymack.com/videos",
        "login_path": "//*[@id='myModal']/div/div/div[2]/div/div/div/div/div[3]/h2/a",
        "tp_path": "//*[@id='body']/div[1]/section/div/div/div[4]/div[1]/div[1]/div/div/span/span[1]",
        "tl_path": "//*[@id='body']/div[1]/section/div/div/div[4]/div[1]/div[1]/div/div/span/span[2]",
        "recaptcha_logo_path": "//*[@id='body']/div[2]/div[4]/div[1]/div[1]",
        "ans_element_tag": "//*[@id='body']/div[1]/section/div/div/div[4]/div[1]/div[3]/ul/li[",
        "is_correct": "//*[@id='body']/div[1]/section/div/div/div[4]/div[3]/div/div[1]/h2"
    },
    {
        "url": "https://www.skylom.com/videos",
        "login_path": "/html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div/div[3]/h2/a",
        "tp_path": "//*[@id='body']/section/div/div/div/div/div/div/div[4]/div[1]/div[1]/div/div/span/span[1]",
        "tl_path": "//*[@id='body']/section/div/div/div/div/div/div/div[4]/div[1]/div[1]/div/div/span/span[2]",
        "recaptcha_logo_path": "//*[@id='body']/div[2]/div[4]/div[1]/div[1]",
        "ans_element_tag": "//*[@id='body']/section/div/div/div/div/div/div/div[4]/div[1]/div[3]/ul/li[",
        "is_correct": "//*[@id='body']/section/div/div/div/div/div/div/div[4]/div[3]/div/div[1]/h2"
    },
    {
        "url": "https://www.flamzy.com/videos",
        "login_path": "//*[@id='myModal']/div/div/div[2]/div/div/div/div/div[3]/h2/a",
        "tp_path": "//*[@id='body']/div[1]/section/div/div/div[4]/div[1]/div[1]/div/div/span/span[1]",
        "tl_path": "//*[@id='body']/div[1]/section/div/div/div[4]/div[1]/div[1]/div/div/span/span[2]",
        "recaptcha_logo_path": "//*[@id='body']/div[2]/div[4]/div[1]/div[1]",
        "ans_element_tag": "//*[@id='body']/div[1]/section/div/div/div[4]/div[1]/div[3]/ul/li[",
        "is_correct": "//*[@id='body']/div[1]/section/div/div/div[4]/div[3]/div/div[1]/h2"
    }
]

CHROME_DRIVER_PATH = '/Users/ishandutta2007/Downloads/chromedriver'
NETWORK_LATENCY = 0
max_thread_id = 0
last_solved_captcha_since_count = 0
is_captcha_waiting = False


def db_lookup(thread_name, conn, vid):
    try:
        q = "SELECT correct_answer from video_answer WHERE video_id='" + vid + "'"
        return conn.cursor().execute(q).fetchone()[0]
    except (TypeError, BaseException) as e:
        print(f'{thread_name}:db_lookup Exception', e)
        return None


def db_write(thread_name, conn, vid, ans):
    try:
        if(db_lookup(thread_name, conn, vid)):
            q = "UPDATE video_answer SET correct_answer='" + \
                ans + "' where video_id='" + vid + "'"
            qs = "Updated"
        else:
            q = "INSERT INTO video_answer VALUES ('" + \
                vid + "', '" + ans + "')"
            qs = "Inserted"
        conn.cursor().execute(q)
        conn.commit()
        print(f'{thread_name}:{qs} Successfully:{vid}:{ans}')
    except BaseException as e:
        print(f'{thread_name}:db_write Exception', e)


def load_cookie(thread_name, driver, path):
    with open(path, 'rb') as cookies_file:
        cookies = pickle.load(cookies_file)
        for cookie in cookies:
            driver.add_cookie(cookie)


def handle_captcha(thread_name, driver, selected_site):
    global is_captcha_waiting
    global last_solved_captcha_since_count
    print(f'{thread_name}:is_captcha_waiting={is_captcha_waiting}:last_solved_captcha_since_count={last_solved_captcha_since_count}')
    if (is_captcha_waiting or last_solved_captcha_since_count == 1):
        raise CustomError
    else:
        is_captcha_waiting = True
        captcha_iframe = driver.find_element_by_xpath(
            "//*[@id='targetForCaptcha1']/div/div/iframe")
        action = ActionChains(driver)
        action.move_to_element(captcha_iframe)
        action.click().perform()
        input(
            f'{thread_name}:Captcha Opened, Press Enter to once you have solved the Captcha.....')
        is_captcha_waiting = False
        last_solved_captcha_since_count = 1


def login(thread_name, driver, selected_site):
    time.sleep(randint(3, 5) + NETWORK_LATENCY)
    try:
        driver.find_element_by_xpath(selected_site['login_path']).click()
    except NoSuchElementException:
        print(f'{thread_name}:You are already logged in')


def get_video_id(thread_name, driver):
    splitted_url = re.split('/|\?|\&|\=', BeautifulSoup(driver.page_source,
                                                        'html.parser').body.find('a', attrs={'class': 'twitter'})['href'])
    return splitted_url[-1].strip()


def start_video(thread_name, driver, selected_site):
    time.sleep(randint(3, 5) + NETWORK_LATENCY)
    try:
        title = driver.find_element_by_xpath("//*[@id='videoTitle']")
        print(f'{thread_name}:Title: {title.text}')
    except NoSuchElementException:
        print(f'{thread_name}:Video Player not found')

    try:
        player_element = driver.find_element_by_css_selector(
            'iframe#video_player')
        player_element.click()
        time.sleep(2)
    except NoSuchElementException:
        print(f'{thread_name}:Video found but Start failed')


def watch_video(thread_name, driver, selected_site):
    time.sleep(randint(2, 4) + NETWORK_LATENCY)
    old_time_remaining = 0
    while(True):
        try:
            time_passed_span = driver.find_element_by_xpath(
                selected_site['tp_path'])
            time_limit_span = driver.find_element_by_xpath(
                selected_site['tl_path'])
            time_remaining = int(time_limit_span.text) - \
                int(time_passed_span.text)
            print(
                f'{thread_name}:Video starts, time remaining: {time_remaining} secs')
            if(time_remaining == old_time_remaining):  # It is stuck, need to click play again
                print(f'{thread_name}:Seems stuck, Trying to start again')
                start_video(thread_name, driver, selected_site)
            time.sleep(time_remaining + randint(2, 5) + NETWORK_LATENCY)
            old_time_remaining = time_remaining
        except (NoSuchElementException, ValueError):
            print(f'{thread_name}:Time completed, let\'s proceed')
            break


def check_correct_answer(thread_name, driver, selected_site):
    time.sleep(randint(2, 3) + NETWORK_LATENCY)
    try:
        corans_ele = driver.find_element_by_xpath(selected_site['is_correct'])
        print(f'{thread_name}:is it Correct? {corans_ele.text}')
        return corans_ele.text.strip().split(' ')[0] == 'Great!'
    except (NoSuchElementException, ElementNotVisibleException):
        print(f'{thread_name}:is it Correct? Correctness can not be figured out')
        return False


def select_new_answer(thread_name, driver, selected_site):
    ans_idx, ans_ele = None, None
    if(ans_idx is None and randint(0, 2) > 0):
        for i in range(1, 5):
            elei = driver.find_element_by_xpath(
                selected_site['ans_element_tag'] + str(i) + "]/a")
            if(elei.text.lower() == "none"):
                ans_idx, ans_ele = i, elei
                break
    if(ans_idx is None and randint(0, 2) > 0):
        for i in range(1, 5):
            elei = driver.find_element_by_xpath(
                selected_site['ans_element_tag'] + str(i) + "]/a")
            if(elei.text.lower() == "vlog"):
                ans_idx, ans_ele = i, elei
                break
    if(ans_idx is None):
        ans_idx = randint(1, 4)
        ans_ele = driver.find_element_by_xpath(
            selected_site['ans_element_tag'] + str(ans_idx) + "]/a")

    return ans_idx, ans_ele


def answer_quiz(thread_name, driver, selected_site, conn):
    vid = get_video_id(thread_name, driver)
    saved_ans = db_lookup(thread_name, conn, vid)
    if (saved_ans is None):
        print(f'{thread_name}:video {vid} is not in DB')
    else:
        print(f'{thread_name}:Saved Answer for video {vid} is {saved_ans}')
    time.sleep(randint(2, 3) + NETWORK_LATENCY)
    while(True):
        ans_idx, ans_ele, update_db = None, None, False
        try:
            if (driver.find_element_by_xpath(
                    selected_site['ans_element_tag'] + str(1) + "]/a").text is None):
                print(f'{thread_name}:Stuck at captcha I guess')
                raise NoSuchElementException
            if(saved_ans is None):
                ans_idx, ans_ele = select_new_answer(
                    thread_name, driver, selected_site)
            else:
                for i in range(1, 5):
                    elei = driver.find_element_by_xpath(
                        selected_site['ans_element_tag'] + str(i) + "]/a")
                    if(elei.text.lower() == saved_ans.lower()):
                        ans_idx, ans_ele = i, elei
                        break
                if(ans_ele is None):
                    print(
                        f'{thread_name}:Something Fishy, Saved Answer {saved_ans} is not matching any of the options {driver.find_element_by_xpath(selected_site["ans_element_tag"]+str(1)+"]/a").text}, {driver.find_element_by_xpath(selected_site["ans_element_tag"]+str(2)+"]/a").text}, {driver.find_element_by_xpath(selected_site["ans_element_tag"]+str(3)+"]/a").text}, {driver.find_element_by_xpath(selected_site["ans_element_tag"]+str(4)+"]/a").text}')
                    ans_idx, ans_ele = select_new_answer(
                        thread_name, driver, selected_site)
                    update_db = True

            ans_ele_text_backup = ans_ele.text
            ans_ele.click()
            print(f'{thread_name}:Option {ans_idx} : {ans_ele_text_backup} clicked')
            is_correct = check_correct_answer(
                thread_name, driver, selected_site)
            if(saved_ans is not None and is_correct == False):
                print(
                    f'{thread_name}:Something Big Fishy, Saved Answer {saved_ans} is now incorrect')
            elif((update_db or saved_ans is None) and is_correct):
                db_write(thread_name, conn, vid, ans_ele_text_backup)
            break
        except (NoSuchElementException, ElementNotVisibleException):
            print(f'{thread_name}:Q/A not found, check for captcha')
            handle_captcha(thread_name, driver, selected_site)


def move_to_next_video(thread_name, driver):
    global last_solved_captcha_since_count
    time.sleep(randint(2, 5) + NETWORK_LATENCY)
    try:
        next_element = driver.find_element_by_css_selector('a#nextvideo')
        next_element.click()
        last_solved_captcha_since_count += 1
        print(f'{thread_name}:----Moving to next(last_solved_captcha_since_count:{last_solved_captcha_since_count})----')
    except NoSuchElementException:
        print(f'{thread_name}:Next element not found')


def move_to_next_pass(thread_name, driver):
    global last_solved_captcha_since_count
    time.sleep(randint(2, 5) + NETWORK_LATENCY)
    try:
        next_pass_element = driver.find_element_by_xpath(
            "//*[@id='body']/section/div/div/div/div[3]/a")
        next_pass_element.click()
        last_solved_captcha_since_count += 1
        print(f'{thread_name}:=====Got 1 Pass({datetime.datetime.now().time()})(last_solved_captcha_since_count:{last_solved_captcha_since_count})=====')
    except NoSuchElementException:
        print(f'{thread_name}:Pass end not yet reached, let\'s continue')


def do(thread_name, selected_site):
    driver = webdriver.Chrome(CHROME_DRIVER_PATH)
    try:
        driver.get(selected_site['url'])
        conn = sqlite3.connect('video.db')
        load_cookie(thread_name, driver, '/tmp/pramolta_fb_cookie')
        login(thread_name, driver, selected_site)
        while(True):
            start_video(thread_name, driver, selected_site)
            watch_video(thread_name, driver, selected_site)
            answer_quiz(thread_name, driver, selected_site, conn)
            move_to_next_video(thread_name, driver)
            if(selected_site['url'] == "https://www.baymack.com/videos"):
                move_to_next_pass(thread_name, driver)
    except (UnexpectedAlertPresentException, WebDriverException, BaseException) as e:
        print(thread_name, e)

    driver.quit()
    conn.close()
    print(f'{thread_name} terminated')


def spawn_threads(selected_site, n):
    global max_thread_id
    for i in range(max_thread_id + 1, max_thread_id + n + 1):
        threading.Thread(target=do, args=[
                         f'Thread-{i}', selected_site]).start()
        time.sleep(randint(5, 15))
        max_thread_id = max(max_thread_id, i)


def main(argv):
    global is_captcha_waiting
    if(len(argv) > 0 and 0 <= int(argv[0]) and int(argv[0]) <= 2):
        selected_site = SITES[int(argv[0])]
    else:
        selected_site = SITES[0]

    if(len(argv) > 1):
        threads = int(argv[1])
    else:
        threads = 2

    conn_for_creation_only = sqlite3.connect('video.db')
    try:
        conn_for_creation_only.cursor().execute(
            '''SELECT name FROM sqlite_master WHERE type='table' AND name=video_answer;''')
    except BaseException as e:
        print(e)
        try:
            conn_for_creation_only.cursor().execute(
                '''CREATE TABLE video_answer (video_id text, correct_answer text)''')
        except BaseException as e:
            print(e)
    conn_for_creation_only.close()

    spawn_threads(selected_site, threads)

    while(threading.activeCount() > 1):
        print(
            f'threading.activeCount: {threading.activeCount()}, is_captcha_waiting: {is_captcha_waiting}, Control C to terminate main : ')
        time.sleep(100)
        if(threading.activeCount() <= 2):
            is_captcha_waiting = False
        if(is_captcha_waiting == False):
            spawn_threads(selected_site,
                          threads - (threading.activeCount() - 1))

    print('Main terminated')


if __name__ == "__main__":
    main(sys.argv[1:])
