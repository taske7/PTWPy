import datetime
import pickle
import re
import sqlite3
import sys
import threading
import time
from random import randint

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException, ElementNotVisibleException, \
    UnexpectedAlertPresentException, WebDriverException
import pyscreenshot as ImageGrab


class StuckException(Exception):
    pass


total_played = 0

SITES = [
    {
        "url": "https://www.baymack.com/videos",
        "login_path": "//*[@id='myModal']/div/div/div[2]/div/div/div/div/div[3]/h2/a",
        "tp_path": "//*[@id='body']/div[1]/section/div/div/div[4]/div[1]/div[1]/div/div/span/span[1]",
        "tl_path": "//*[@id='body']/div[1]/section/div/div/div[4]/div[1]/div[1]/div/div/span/span[2]",
        "recaptcha_logo_path": "//*[@id='body']/div[2]/div[4]/div[1]/div[1]",
        "ans_element_tag": "//*[@id='body']/div[1]/section/div/div/div[4]/div[1]/div[3]/ul/li[",
        "is_correct": "//*[@id='body']/div[1]/section/div/div/div[4]/div[3]/div/div[1]/h2"
    },
    {
        "url": "https://www.skylom.com/videos",
        "login_path": "/html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div/div[3]/h2/a",
        "tp_path": "//*[@id='body']/section/div/div/div/div/div/div/div[4]/div[1]/div[1]/div/div/span/span[1]",
        "tl_path": "//*[@id='body']/section/div/div/div/div/div/div/div[4]/div[1]/div[1]/div/div/span/span[2]",
        "recaptcha_logo_path": "//*[@id='body']/div[2]/div[4]/div[1]/div[1]",
        "ans_element_tag": "//*[@id='body']/section/div/div/div/div/div/div/div[4]/div[1]/div[3]/ul/li[",
        "is_correct": "//*[@id='body']/section/div/div/div/div/div/div/div[4]/div[3]/div/div[1]/h2"
    },
    {
        "url": "https://www.flamzy.com/videos",
        "login_path": "//*[@id='myModal']/div/div/div[2]/div/div/div/div/div[3]/h2/a",
        "tp_path": "//*[@id='body']/div[1]/section/div/div/div[4]/div[1]/div[1]/div/div/span/span[1]",
        "tl_path": "//*[@id='body']/div[1]/section/div/div/div[4]/div[1]/div[1]/div/div/span/span[2]",
        "recaptcha_logo_path": "//*[@id='body']/div[2]/div[4]/div[1]/div[1]",
        "ans_element_tag": "//*[@id='body']/div[1]/section/div/div/div[4]/div[1]/div[3]/ul/li[",
        "is_correct": "//*[@id='body']/div[1]/section/div/div/div[4]/div[3]/div/div[1]/h2"
    },
    {
        "url": "https://www.snuckls.com/videos",
        "login_path": "/html/body/div[1]/div[2]/div/div/div[2]/div/div/div/div/div[3]/h2/a",
        "tp_path": "//*[@id='body']/section/div/div/div/div/div/div/div[4]/div[1]/div[1]/div/div/span/span[1]",
        "tl_path": "//*[@id='body']/section/div/div/div/div/div/div/div[4]/div[1]/div[1]/div/div/span/span[2]",
        "recaptcha_logo_path": "//*[@id='body']/div[2]/div[4]/div[1]/div[1]",
        "ans_element_tag": "//*[@id='body']/section/div/div/div/div/div/div/div[4]/div[1]/div[3]/ul/li[",
        "is_correct": "//*[@id='body']/section/div/div/div/div/div/div/div[4]/div[3]/div/div[1]/h2"
    }
]

CHROME_DRIVER_PATH = '/Users/ishandutta2007/Downloads/chromedriver'
NETWORK_LATENCY = 0
max_thread_id = 0


def db_lookup(conn, vid):
    try:
        q = "SELECT correct_answer from video_answer WHERE video_id='" + vid + "'"
        return conn.cursor().execute(q).fetchone()[0]
    except (TypeError, BaseException) as e:
        print(f'db_lookup Exception', e)
        return None


def db_write(conn, vid, ans):
    try:
        if db_lookup(conn, vid):
            q = "UPDATE video_answer SET correct_answer='" + \
                ans + "' where video_id='" + vid + "'"
            qs = "Updated"
        else:
            q = "INSERT INTO video_answer VALUES ('" + \
                vid + "', '" + ans + "')"
            qs = "Inserted"
        conn.cursor().execute(q)
        conn.commit()
        print(f'{qs} Successfully:{vid}:{ans}')
    except BaseException as e:
        print(f'db_write Exception', e)


def load_cookie(driver, path):
    with open(path, 'rb') as cookies_file:
        cookies = pickle.load(cookies_file)
        for cookie in cookies:
            driver.add_cookie(cookie)


def handle_captcha(driver):
    global total_played
    try:
        captcha_iframe = driver.find_element_by_xpath(
            "//*[@id='targetForCaptcha1']/div/div/iframe")
        driver.switch_to_window(driver.current_window_handle)
        driver.set_window_size(400, 800)
        print(f'Captcha Encountered')

        action = ActionChains(driver)
        action.move_to_element(captcha_iframe)
        action.click().perform()
        print(f'Captcha Opened')

        im = ImageGrab.grab(bbox=(0, 140, 400, 710))
        im.save(f'tmp/last_recaptcha.png')
        print(f'Screenshot saved')

        while(captcha_iframe.is_displayed()):
            print("Still waiting for captcha solution.....")
            time.sleep(randint(13, 15) + NETWORK_LATENCY)
        print(f'Seems Captcha has been solved, let\'s proceed')
        total_played = 0
        driver.set_window_size(0, 600)
    except BaseException as e:
        print(f'It was non clickable captcha, lets proceed', e)
        raise e


def login(driver, selected_site_index):
    time.sleep(randint(3, 5) + NETWORK_LATENCY)
    try:
        driver.find_element_by_xpath(
            SITES[selected_site_index]['login_path']).click()
    except NoSuchElementException as e:
        print(f'You are already logged in', e)


def get_video_id(driver):
    splitted_url = re.split('/|\?|\&|\=',
                            BeautifulSoup(driver.page_source, 'html.parser').body.find('a', attrs={'class': 'twitter'})[
                                'href'])
    print(f'Got vid_id: {splitted_url[-1].strip()}')
    return splitted_url[-1].strip()


def start_video(driver, selected_site_index):
    global total_played
    time.sleep(randint(3, 5) + NETWORK_LATENCY)
    try:
        title = driver.find_element_by_xpath("//*[@id='videoTitle']")
        print(f'Title: {title.text}')
    except NoSuchElementException as e:
        print(f'Video Player not found', e)

    try:
        player_element = driver.find_element_by_css_selector(
            'iframe#video_player')
        player_element.click()
        time.sleep(2)
    except NoSuchElementException as e:
        print(f'Video found but Start failed', e)
    total_played += 1
    print(f'Play clicked.total_played={total_played}')


def watch_progressbar(driver, selected_site_index):
    js = "$('.video-box').remove();"
    time.sleep(randint(2, 4) + NETWORK_LATENCY)
    old_time_remaining = 0
    while (True):
        try:
            time_passed_span = driver.find_element_by_xpath(
                SITES[selected_site_index]['tp_path'])
            time_limit_span = driver.find_element_by_xpath(
                SITES[selected_site_index]['tl_path'])
            time_remaining = int(time_limit_span.text) - \
                int(time_passed_span.text)

            ctr = 0
            while int(time_passed_span.text) == 0:
                time.sleep(1)
                ctr += 1
                print("Waiting for video to start", ctr)
                if ctr >= 50:
                    print("Waited too long , let's restart")
                    raise StuckException
            driver.execute_script(js)

            print(
                f'Progressbar starts, video hides, time remaining: {time_remaining} secs')
            if time_remaining == old_time_remaining:
                print(f'Seems stuck, Trying to start again')
                raise StuckException
            time.sleep(time_remaining + randint(2, 5) + NETWORK_LATENCY)
            old_time_remaining = time_remaining
        except (NoSuchElementException, ValueError) as e:
            print(f'Time completed, let\'s proceed', e)
            break
    print("Watched progressbar")


def check_correct_answer(driver, selected_site_index):
    time.sleep(randint(2, 3) + NETWORK_LATENCY)
    try:
        corans_ele = driver.find_element_by_xpath(
            SITES[selected_site_index]['is_correct'])
        print(f'Is it Correct? {corans_ele.text}')
        return corans_ele.text.strip().split(' ')[0] == 'Great!'
    except (NoSuchElementException, ElementNotVisibleException) as e:
        print(f'Is it Correct? Correctness can not be figured out', e)
        return False


def select_new_answer(driver, selected_site_index):
    print("New Ans case: quiz loaded")
    ans_idx, ans_ele = None, None
    if ans_idx is None and randint(0, 2) > 0:
        print("Let's see if it has none")
        for i in range(1, 5):
            elei = driver.find_element_by_xpath(
                SITES[selected_site_index]['ans_element_tag'] + str(i) + "]/a")
            if elei.text.lower() == "none":
                ans_idx, ans_ele = i, elei
                break
    if ans_idx is None and randint(0, 2) > 0:
        print("Let's see if it has vlog")
        for i in range(1, 5):
            elei = driver.find_element_by_xpath(
                SITES[selected_site_index]['ans_element_tag'] + str(i) + "]/a")
            if elei.text.lower() == "vlog":
                ans_idx, ans_ele = i, elei
                break
    if ans_idx is None and randint(0, 2) > 0:
        print("Let's see if it has hip hop")
        for i in range(1, 5):
            elei = driver.find_element_by_xpath(
                SITES[selected_site_index]['ans_element_tag'] + str(i) + "]/a")
            if elei.text.lower() == "hip hop":
                ans_idx, ans_ele = i, elei
                break
    if ans_idx is None:
        print("Let's just randomly choose one")
        ans_idx = randint(1, 4)
        ans_ele = driver.find_element_by_xpath(
            SITES[selected_site_index]['ans_element_tag'] + str(ans_idx) + "]/a")
    print(f'ans_idx : {ans_idx}')

    return ans_idx, ans_ele


def answer_quiz(driver, selected_site_index, conn, vid, care_captch):
    saved_ans = db_lookup(conn, vid)
    if saved_ans is None:
        print(f'Video {vid} is not in DB')
    else:
        print(f'Saved Answer for video {vid} is {saved_ans}')
    time.sleep(randint(2, 3) + NETWORK_LATENCY)
    while (True):
        ans_idx, ans_ele, update_db = None, None, False
        try:
            if driver.find_element_by_xpath(
                    SITES[selected_site_index]['ans_element_tag'] + str(1) + "]/a").text is None:
                print(f'Stuck at captcha I guess')
                raise NoSuchElementException
            if saved_ans is None:
                elei = driver.find_element_by_xpath(
                    SITES[selected_site_index]['ans_element_tag'] + str(1) + "]/a")
                if elei.text.strip() == "":
                    raise ElementNotVisibleException
                else:
                    ans_idx, ans_ele = select_new_answer(
                        driver, selected_site_index)
            else:
                print("Saved Ans case")
                for i in range(1, 5):
                    elei = driver.find_element_by_xpath(
                        SITES[selected_site_index]['ans_element_tag'] + str(i) + "]/a")
                    if elei.text.lower() == saved_ans.lower():
                        ans_idx, ans_ele = i, elei
                        break
                if ans_ele is None:
                    o1 = driver.find_element_by_xpath(
                        SITES[selected_site_index]["ans_element_tag"] + str(1) + "]/a").text
                    o2 = driver.find_element_by_xpath(
                        SITES[selected_site_index]["ans_element_tag"] + str(2) + "]/a").text
                    o3 = driver.find_element_by_xpath(
                        SITES[selected_site_index]["ans_element_tag"] + str(3) + "]/a").text
                    o4 = driver.find_element_by_xpath(
                        SITES[selected_site_index]["ans_element_tag"] + str(4) + "]/a").text
                    print(
                        f'Something Fishy, Saved Answer {saved_ans} is not matching any of the options {o1}, {o2}, {o3}, {o4}')
                    ans_idx, ans_ele = select_new_answer(
                        driver, selected_site_index)
                    update_db = True

            ans_ele_text_backup = ans_ele.text
            ans_ele.click()
            print(f'Option {ans_idx} : {ans_ele_text_backup} clicked')
            is_correct = check_correct_answer(driver, selected_site_index)
            if saved_ans is not None and is_correct == False:
                print(
                    f'Something Big Fishy, Saved Answer {saved_ans} is now incorrect')
            elif (update_db or saved_ans is None) and is_correct:
                db_write(conn, vid, ans_ele_text_backup)
            print("Breaking answer's loop")
            break
        except (NoSuchElementException, ElementNotVisibleException) as e:
            print(f'Q/A not found, check for captcha', e)
            if not care_captch:
                driver.quit()
                conn.close()
                print(f'do_once terminated')
            handle_captcha(driver)
    print('Answered, now will try next vid(or answer again if bug comes up)')


def move_to_next_video(driver):
    time.sleep(randint(2, 5) + NETWORK_LATENCY)
    try:
        next_element = driver.find_element_by_css_selector('a#nextvideo')
        print('next_element:', next_element,
              'next_element.text:', next_element.text)
        if next_element.is_displayed():
            next_element.click()
            print('next_element clicked')
        else:
            print(
                "Waiting since next_element not visible, maybe you need to take quiz again")
        print(f'----Moving to next----')
    except NoSuchElementException as e:
        print(f'Next element not found', e)


def move_to_next_pass(driver):
    time.sleep(randint(2, 5) + NETWORK_LATENCY)
    try:
        next_pass_element = driver.find_element_by_xpath(
            "//*[@id='body']/section/div/div/div/div[3]/a")
        next_pass_element.click()
        print(f'=====Got 1 Pass({datetime.datetime.now().time()})=====')
    except NoSuchElementException as e:
        print(f'Pass end not yet reached, let\'s continue', e)


def dodo(selected_site_index, driver, conn, care_captch=True):
    print("New Video started")
    vid = get_video_id(driver)
    start_video(driver, selected_site_index)
    watch_progressbar(driver, selected_site_index)
    while(1):
        try:
            answer_quiz(driver, selected_site_index, conn, vid, care_captch)
        except Exception as e:
            print('Seems alert one.', e)
            raise e
        next_element = driver.find_element_by_css_selector('a#nextvideo')
        if next_element.is_displayed():
            move_to_next_video(driver)
        elif selected_site_index in [0, 2]:
            move_to_next_pass(driver)
        time.sleep(5)
        try:
            player = driver.find_element_by_css_selector('iframe#video_player')
            if player.is_displayed():
                print('Seems new video started')
                break
        except Exception as e:
            print('New video not loaded means still quiz loop continues', e)
    print('Out of quizzing buggy loop')


def do(selected_site_index):
    driver = webdriver.Chrome(CHROME_DRIVER_PATH)
    driver.set_window_size(0, 600)
    driver.set_window_position((230 * 0) % (230 * 4), 0)
    try:
        driver.get(SITES[selected_site_index]['url'])
        conn = sqlite3.connect('video.db')
        load_cookie(driver, '/tmp/pramolta_fb_cookie')
        login(driver, selected_site_index)
        while (True):
            dodo(selected_site_index, driver, conn)
    except (UnexpectedAlertPresentException, WebDriverException, BaseException) as e:
        print('do', e)

    driver.quit()
    conn.close()
    print(f'do terminated')


def do_once(selected_site_index, window_no):
    driver = webdriver.Chrome(CHROME_DRIVER_PATH)
    driver.set_window_size(0, 600)
    driver.set_window_position((230 * (0 + window_no)) % (230 * 4), 0)
    try:
        driver.get(SITES[selected_site_index]['url'])
        conn = sqlite3.connect('video.db')
        load_cookie(driver, '/tmp/pramolta_fb_cookie')
        login(driver, selected_site_index)
        driver.set_window_position(-500, 0)
        driver.set_window_size(0, 0)
        dodo(selected_site_index, driver, conn, False)
    except (UnexpectedAlertPresentException, WebDriverException, BaseException) as e:
        print('do_once', e)

    driver.quit()
    conn.close()
    print(f'Auxilary Window No:{window_no}:do_once terminated')


def main(argv):
    d = {}
    for i in range(0, len(argv), 2):
        d[argv[i].replace('-', '')] = argv[i + 1]

    if 's' in d:
        selected_site_index = int(d['s'])
    else:
        selected_site_index = 0

    conn_for_creation_only = sqlite3.connect('video.db')
    try:
        conn_for_creation_only.cursor().execute(
            '''SELECT name FROM sqlite_master WHERE type='table' AND name=video_answer;''')
    except BaseException as e:
        print(e)
        try:
            conn_for_creation_only.cursor().execute(
                '''CREATE TABLE video_answer (video_id text, correct_answer text)''')
        except BaseException as e:
            print(e)
    conn_for_creation_only.close()
    while(True):
        if(threading.activeCount() <= 1):
            threading.Thread(target=do, args=[selected_site_index]).start()
            time.sleep(20)
        if(threading.activeCount() <= 2 and total_played < 2):
            threading.Thread(target=do_once, args=[
                             selected_site_index, 1]).start()
            time.sleep(15)
        if(threading.activeCount() <= 3 and total_played < 3):
            threading.Thread(target=do_once, args=[
                             selected_site_index, 2]).start()
            time.sleep(15)
        if(threading.activeCount() <= 4 and total_played < 4):
            threading.Thread(target=do_once, args=[
                             selected_site_index, 3]).start()
            time.sleep(15)
    print('Main terminated')


if __name__ == "__main__":
    main(sys.argv[1:])
