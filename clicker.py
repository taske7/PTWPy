import pyautogui
import time
from random import randint
pyautogui.FAILSAFE = False
HACK_CONST = 1.3


def navigate_from_to(idx, fro, to):
    global HACK_CONST
    if to == 'verify':
        HACK_CONST = 1.0
    if fro != to:
        x = list()
        y = list()
        x0, y0 = pyautogui.position()
        ctr = 0
        with open(f'3dim_offset/3dim_{fro}_to_{to}_offset.txt') as infile:
            for line in infile:
                if ctr % 8000 == 1:
                    xoff, yoff = line[1:-2].partition(",")[::2]
                    x.append(x0 + int(xoff) * HACK_CONST)
                    y.append(y0 + int(yoff) * HACK_CONST)
                ctr += 1
        for i in range(0, len(x)):
            pyautogui.moveTo(x[i], y[i])
    if idx == 0:
        pyautogui.click()
        time.sleep(randint(1, 2))
    pyautogui.click()


def check_selected(process_no, dimension, to_be_checked, last_loc, isDone):  # 3x3 or 4x4
    navigate_from_to(0, last_loc, to_be_checked[0])
    for i in range(0, len(to_be_checked) - 1):
        navigate_from_to(i + 1, to_be_checked[i], to_be_checked[i + 1])
    if isDone == 1:
        navigate_from_to(
            100000, to_be_checked[len(to_be_checked) - 1], 'verify')
        print('verified')


while(True):
    process_no = int(input("process_no(0 to 3):") or 0)
    dim = int(input("dimension(3 or 4):") or 3)
    to_be_checked = list(
        map(int, input(f'Enter Boxes to be checked(1-{dim*dim}):').split()))
    current_loc = int(input("current_loc(1 to 9):"))
    isDone = int(input("0 or 1:") or 0)
    check_selected(process_no, dim, to_be_checked, current_loc, isDone)
